/** 
 * @file mediastream.h
 * @brief 媒体格式定义
 * @author Chenkai
 * @date 2007-11-20
 */

#ifndef __MEDIASTREAM_H__
#define __MEDIASTREAM_H__


#ifdef __cplusplus
extern "C"
{
#endif

#include "swapi.h" 

typedef enum {
    VIDEOCAP_TYPE_NULL,
	VIDEOCAP_TYPE_HD,		/**<高清解码器*/
	VIDEOCAP_TYPE_SD,		/**<标清解码器*/
	VIDEOCAP_TYPE_MOSAIC,   /**<马赛克解码器*/
	VIDEOCAP_TYPE_IMAGE,    /**<图片解码器*/
	VIDEOCAP_TYPE_MAX
} e_videocap_type;

typedef struct _mediacap 
{
    int video;
    int audio;
    int recorder;
} mediacap_t;


//定义流里最多同时拥有的视频流或者音频流或者subtitle的数目
#define SW_MAXSTREAMS 16

//多媒体数据在程序内部传送的模式, 每个对应一种playback
typedef enum
{
    STREAM_NULL = 0,
    STREAM_DATA,    /*数据传送, swtream接收数据送给swdecoder*/
    STREAM_DVB,     /*DVB方式, swstream只是告诉swdecoder DVB频点信息，decoder自己处理*/
    STREAM_DVB_USB, /*如果与STREAM_DVB使用相同的playback, 需将此playback同时注册到两个槽位*/
    STREAM_MOSAIC,  /*MOSAIC方式, playback自己处理*/
    STREAM_FILE,  /*fileplayback*/
    STREAM_MAX
} stream_mode_t;

typedef enum
{
    STRMCTRL_NULL = 0,
    STRMCTRL_DUMMY,
    STRMCTRL_RTMP,
    STRMCTRL_FFMPEG,
    STRMCTRL_FILE,
    //STRMCTRL_MP3,

    STRMCTRL_RTSP,
    STRMCTRL_SUNNIWELL,
    STRMCTRL_MDN,
    STRMCTRL_CTC,
    STRMCTRL_DARWIN,
    //STRMCTRL_FREEBOX,
    STRMCTRL_NCUBE,
    STRMCTRL_SFOUNT,

    STRMCTRL_WMS,
    STRMCTRL_WMV,
    STRMCTRL_NSC,

    STRMCTRL_DVB,
    STRMCTRL_MOSAIC,

    STRMCTRL_SHOUTCAST,
	
	STRMCTRL_HLS,
	STRMCTRL_SMTH,
    //STRMCTRL_MAX

} e_strmctrl_type;


typedef enum
{
    PACKTYPE_NULL = 0,
    PACKTYPE_RTP,
    PACKTYPE_RAW,
} packtype_t;

typedef enum
{
    RECVTYPE_NULL = 0,
    RECVTYPE_TCP,
    RECVTYPE_UDP,
} recvtype_t;


//流类型
typedef enum
{
    SYSTEM_STREAM = 0,
    VIDEO_STREAM,
    AUDIO_STREAM
} stream_type_t;


//rtp格式类型
typedef enum
{
    RTP_NULL = 0,    //没有RTP
    RTP_MP2T,
    RTP_MPEG4,
    RTP_MP4GENERIC,
    RTP_H264,
    RTP_ASF
} rtp_type_t;

typedef enum
{
    SYSTEM_NULL = 0,
    SYSTEM_MP2T = 0x01,
    SYSTEM_MP2P = 0x02,
    SYSTEM_DVD  = 0x04,
    SYSTEM_ASF  = 0x08,
    SYSTEM_AVI  = 0x10,
    SYSTEM_MP1P = 0x20,
    SYSTEM_MP4  = 0x40,
    SYSTEM_BRCM_RECD = 0x80,
    SYSTEM_FLV  = 0x100,
    SYSTEM_MKV  = 0x200,
    SYSTEM_BDAV = 0x400,
	SYSTEM_RM = 0x800
} system_type_t;

typedef enum
{
    CODEC_UNKNOWN = 0,

    VIDEO_FIRST = 0x1000,
    VIDEO_AVS = VIDEO_FIRST,
    VIDEO_DIVX,
    VIDEO_FLV, //sorenson h263
    VIDEO_H263,
    VIDEO_H264,
    VIDEO_MJPEG,
    VIDEO_MP1V,
    VIDEO_MP2V,
    VIDEO_MP4V,
    VIDEO_RV10,
    VIDEO_RV20,
    VIDEO_RV30,
    VIDEO_RV40,
    VIDEO_THEORA,
    VIDEO_VC1,
    VIDEO_VP6,
    VIDEO_VP6F,
    VIDEO_VP6A,
    VIDEO_VP8,
    VIDEO_WMV1,
    VIDEO_WMV2,
    VIDEO_WMV3,
    VIDEO_H265,
    VIDEO_LAST = VIDEO_H265,

    AUDIO_FIRST = 0x2000,
    AUDIO_ALAC = AUDIO_FIRST,
    AUDIO_AAC,
    AUDIO_AAC_PLUS,
    AUDIO_AC3,
    AUDIO_AC3_PLUS,
	AUDIO_ATRAC3,
    AUDIO_FLAC,
    AUDIO_APE,
    AUDIO_COOK,
    AUDIO_DTS,
    AUDIO_GSM,
    AUDIO_MP3, 
    AUDIO_MPEG,
    AUDIO_RA144,
    AUDIO_RA288,
    AUDIO_VORBIS,
    AUDIO_WMA1,
    AUDIO_WMA2,
    AUDIO_WMAPRO,
	AUDIO_TRUEHD,
	AUDIO_AMR_NB,

    AUDIO_DPCM = 0x2100,
    AUDIO_ADPCM,
    AUDIO_PCM,
    AUDIO_PCM_S16LE,
    AUDIO_PCM_S16BE,
    AUDIO_PCM_BLURAY,
    AUDIO_LAST = AUDIO_PCM_BLURAY,

    SUBTITLE_FIRST = 0x3000,
    SUBTITLE_DVD = SUBTITLE_FIRST,
    SUBTITLE_DVB,
    SUBTITLE_PGS,
    SUBTITLE_SRT,
    SUBTITLE_SRTE, //mkv等内嵌的srt字幕
    SUBTITLE_LRC,
    SUBTITLE_SSA,
    SUBTITLE_SMI,
    SUBTITLE_VOBSUB,
    SUBTITLE_LAST = SUBTITLE_VOBSUB,
} swcodec_type_t;


typedef enum
{
    SW_PIXFMT_UNKNOWN = 0,

    SW_PIXFMT_RGB8888,
    SW_PIXFMT_RGB565,

    SW_PIXFMT_YUV420Planar,
    SW_PIXFMT_YUV420SemiPlanar,
    SW_PIXFMT_YUV422Planar,
    SW_PIXFMT_YUV422SemiPlanar,

    SW_PIXFMT_PLATFORM_DEPENDENT,

    SW_PIXFMT_MAX,

} swpixelformat_e;


typedef enum
{
    SW_AUDIOFMT_UNKNOWN,

    SW_AUDIOFMT_U8,
    SW_AUDIOFMT_S8,

    SW_AUDIOFMT_U16LE,
    SW_AUDIOFMT_U16BE,
    SW_AUDIOFMT_S16LE,
    SW_AUDIOFMT_S16BE,

    SW_AUDIOFMT_S24LE,
    SW_AUDIOFMT_S24BE,

    SW_AUDIOFMT_S32LE,
    SW_AUDIOFMT_S32BE,

    //----------------

    SW_AUDIOFMT_FLT, //float
    SW_AUDIOFMT_DBL, //double

    SW_AUDIOFMT_U8P,
    SW_AUDIOFMT_S16P,
    SW_AUDIOFMT_S32P,
    SW_AUDIOFMT_FLTP, //float, planar
    SW_AUDIOFMT_DBLP, //double, planar

    SW_AUDIOFMT_MAX,
} swaudio_format_e;


typedef struct system_stream
{
    rtp_type_t rtp_type;
    uint16_t   program_number;
    uint16_t   pmt_pid;
    uint16_t   pcr_pid;
    uint32_t   codec;       //系统编码格式
    uint32_t   clock_rate;  //some ctrl's sdp parse functions assign clockrate to system.
    uint8_t    *spec_data;  //decoder config
    uint32_t   spec_len;    //decoder config size
} system_stream_t;


typedef struct video_stream
{
    rtp_type_t   rtp_type;
    int          rtp_timebase;
    int          rtp_strmid;
    uint16_t     pid;
    swcodec_type_t codec;     //视频格式
    uint32_t     clock_rate; 
    uint8_t      *spec_data;//decoder config
    int          spec_len;  //decoder config size

    int frame_width;
    int frame_height;
    swpixelformat_e pixel_format;
} video_stream_t;


typedef struct audio_stream
{
    rtp_type_t   rtp_type;
    int          rtp_timebase;
    int          rtp_strmid;
    uint16_t     pid;
    swcodec_type_t codec;        //audio 类型
    uint32_t     clock_rate;   //some ctrl's sdp parse functions assign clockrate to audio.
    uint16_t     profile;      //profile level id
    uint8_t      layer;        //音频layer
    char         language[4];  //语言类型
    uint8_t      *spec_data;        
    int          spec_len;

    uint32_t     samplerate;   //audio采样率
    uint8_t      channels;     //音频几个声道
    swaudio_format_e sample_format;
    uint32_t     bitrate;
	uint32_t     block_align;
    int frame_size;
} audio_stream_t;


typedef struct subtitle_stream
{
    swcodec_type_t subtitle_type; //subtitle类型
    void *type_param; //与具体type相关的信息. SUBTITLE_SRT, SUBTITLE_LRC时将文件名存放于此       
    char language[4]; //subtitle语言
    uint16_t pid;            
    uint16_t com_page_id;
    uint16_t anc_page_id;
} subtitle_stream_t;

//描述stream源信息
typedef struct stream_source
{
    char url[1024];
    union
    {
        struct
        {
            uint32_t ip;
			struct in6_addr ipv6;
            uint16_t ports[SW_MAXSTREAMS];
        }udp;
		unsigned int inputband;	/**< for dvb to decide which tuner to use */
    }source;
    int64_t filesize;
} stream_source_t;


//流播放时间范围
typedef struct stream_range
{
    uint32_t start; //开始播放的时间
    uint32_t end;   //停止播放的时间
} stream_range_t;

typedef struct media_stream
{
    stream_source_t     source;                     //流发送源信息
    stream_mode_t       mode;                       //流接收模式
    stream_range_t      range;                      //session range
    packtype_t          packtype;
    recvtype_t          recvtype;
    e_strmctrl_type     strmctrl_type;
                                           
    uint16_t            system_num;                 //系统流的数目,只支持0或者1
    system_stream_t     system;                     //系统流格式
                                           
    uint16_t            video_num;                  //视频流个数
    video_stream_t      videos[SW_MAXSTREAMS];      //视频格式
    int                 video_index;                //<ysn-added>
                                           
    uint16_t            audio_num;                  //音频流个数
    audio_stream_t      audios[SW_MAXSTREAMS];      //音频格式
    int                 audio_index;                                 

    uint16_t            subtitle_num;               //字幕个数
    subtitle_stream_t   subtitles[SW_MAXSTREAMS];   //字幕格式
    int                 subtitle_index;

    uint16_t            teletext_num;               //图文个数
    uint16_t            teletext_pid[SW_MAXSTREAMS];//图文pid
    int                 teletext_index;

    char                *ca_drm;                    //流所使用加密的CA或者DRM名称
    uint16_t            ecm_num;
    uint16_t            ecm_pid[SW_MAXSTREAMS];     //CA_pid
    uint16_t            ca_system_id;
    
	char 				record_dir[1024];
	bool				is_m3u8;

} media_stream_t;


#ifdef __cplusplus
}
#endif

#endif //__MEDIASTREAM_H__
